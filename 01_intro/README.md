
---

# Introduction

---
# Install and Configuration
To install Groovy, Java development environment is required. Java's libraries are used as part of Groovy and without them it will not work. Groovy itself can be installed wither with popular 3rd party tool called `sdkman` or by downloading and install the groovy binary into your system under `/bin` or `~/bin` folders.
Below is an example of Groovy and Gradle install on any Debian Linux based system (Debian, Ubuntu, Linux, MxLinux...)
```sh
 sudo apt install -y default-jdk
 curl -s "https://get.sdkman.io" | bash 
 bash # to start new shell session
 sdk install groovy
 sdk install gradle
```

In case your working with RedHat based system (RedHat, Fedora, Rocky, Alma), the steps are not that different, with exception on installing Java development environment.

```sh
 sudo dnf install -y java-latest-openjdk.x86_64
 curl -s "https://get.sdkman.io" | bash 
 bash # to start new shell session
 sdk install groovy
 sdk install gradle
```

In case you are on MacOS `brew` can provide you with a version of `Gradle` with dependencies, but in case you prefer latest version, then `sdkman` is the tool for your use.
```sh
brew install gradle
# or for newer version
curl -s "https://get.sdkman.io" | bash 
bash # to start new shell session
sdk install groovy
sdk install gradle
```

If you are on Windows, you are welcome to try to WSL and use Debian based version or [address documentation to read about manually setup](https://docs.gradle.org/current/userguide/installation.html#ex-installing-manually). 

To verify the installation you can use shell/console to check about your details:

```sh
$ gradle -v

Welcome to Gradle 8.4!

Here are the highlights of this release:
 - Compiling and testing with Java 21
 - Faster Java compilation on Windows
 - Role focused dependency configurations creation

For more details see https://docs.gradle.org/8.4/release-notes.html


------------------------------------------------------------
Gradle 8.4
------------------------------------------------------------

Build time:   2023-10-04 20:52:13 UTC
Revision:     e9251e572c9bd1d01e503a0dfdf43aedaeecdc3f

Kotlin:       1.9.10
Groovy:       3.0.17
Ant:          Apache Ant(TM) version 1.10.13 compiled on January 4 2023
JVM:          17.0.8 (Debian 17.0.8+7-Debian-1deb12u1)
OS:           Linux 6.1.0-13-amd64 amd64

```
---


# Documentation

The docs can be found at [gradle web-site](https://docs.gradle.org)

---

# Where to find the answers

Somewhere between [forum](https://discuss.gradle.org) and stackoverflow

