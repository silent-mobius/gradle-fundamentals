# Gradle Fundamentals

Course for learning basics of gradle use in Java based projects

### About The Course Itself
We'll learn several topics mainly focused on:
- What is gradle ?
- Who needs gradle ?
- How gradle works ?
- How to manage gradle in various environments


### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of gradle automation
- For junior/senior developers who are developing on Java stacks and need setup automation with gradle
- Experienced ops who need refresher automating and deploying



### Course Topics

- Introduction
- Learning Enough Groovy To Manage
- Basic Project Build
- Custom Tasks
- Miscelenious tasks
- Plugins
- Larger Project Examples

> `[!]` Note: Please use build.sh script to create html version for your use


© All Right reserved to Alex M. Schapelle of VaioLabs ltd.
Copyright (C) 2023  VaioLabs ltd
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.