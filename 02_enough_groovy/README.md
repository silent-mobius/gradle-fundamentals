
---

# Enough Groovy To Manage

- `GroovyConsule`
- can work in console for testing code snippets
- can use java syntax but not necessary
- OPTIONALLY Typed Language
    - Interpreter will try to determine the type of data that is use

---

# Static Vs. Dynamic Typing and Basic Syntax
Groovy considered `Optionally Type Language`, meaning that if you know `Java` you can utilities its language structure, but with more easy and with less semantic emphasis:
- Everything mandatory in `Java` is optional in `Groovy`

```groovy
// single line comment
/*
multi line comment
*/

def name='Alex' //def is short for define
String name2='Michael' // we can specify type of the specifically if we wish to -> types are String, Int, Float, Bool and many more
println  ('Hello World!');
println "Hello ${name}!" // -> even shorter syntax without ; and () -> "" are required for interpolation
println "Hello ${name} and $name2 !" // -> interpolation can be used with or without {}

def say_hello(){ // this function/routine/method -> yes we use def for it as well
    "Hello World!"
}

def say_hello(msg='Hello', name='World'){ // we can pass it values
    "$msg $name!"
}

// Functions can be called in several ways
assert 'Hello World!' == say_hello()
assert 'Hi world!' == say_hello('Hi')
assert 'learn groovy!' == say_hello('learn', 'groovy')


//Groovy supports the usual if - else syntax
def x = 3

if(x==1) {
    println "One"
} else if(x==2) {
    println "Two"
} else {
    println "X greater than Two"
}

```
---

# POGO : Plain Old Groovy Objects

- In Groovy most of Java and Groovy libs imported
    - You can import manually, something specific, but mostly it is not required

```groovy 
class Person{
    String first
    String last

    // In groovy the properties are RETURNED Automatically
    // Everything is Public and has automatic setters/getter and many more
    // Unless specified manually
}

Person qb = new Person()
qb.setFirst('Alex') // In Groovy automatically creates public setters/getters
qb.last = 'Schapelle' // We can also access values directly into class propertry just line in python
// In Groovy you can specify properties in class and it will assign values
Person qb = new Person(first: 'Alex', last: 'Schapelle')
println "${qb.getFirst()} ${qb.last}" 
```

---

# Collections and Clousures

- Groovy Closure is like a "code block" or a method pointer. It is a piece of code that is defined and then executed at a later point.
```groovy 
//For loop in Groovy --> Note that in DSL it might not always work !!!
//Iterate over a range
def x = 0
for (i in 0 .. 30) {
    x += i
}

//Iterate over a list
x = 0
for( i in [5,3,2,1] ) {
    x += i
}

//Iterate over an array
array = (0..20).toArray()
x = 0
for (i in array) {
    x += i
}

//Iterate over a map
def map = ['name':'Roberto', 'framework':'Grails', 'language':'Groovy']
x = ""
for ( e in map ) {
    x += e.value
    x += " "
}
assert x.equals("Roberto Grails Groovy ")

```